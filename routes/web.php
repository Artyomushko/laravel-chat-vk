<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::auth();

Route::group(['middleware' => 'auth'], function (){
    Route::get('/chat', 'ChatController@index')->name('chat.index');
    Route::get('/chat/{user}');
    Route::post('/rate/message/{message}');
    Route::post('/rate/user/{user}');

    Route::post('/logout', function(){Auth::logout();})->name('auth.logout');
});

//    Route::get()
Route::get('/', function () {
    return view('welcome');
});
Route::get('/login/vk', 'LoginVkontakteController@redirectToVk')->name('login.vk');
Route::get('/login/vk/user', 'LoginVkontakteController@VkCallback')->name('login.vk.callback');
