<li class="list-group-item d-flex justify-content-between align-items-center" data-user-id="{{ $user->id }}">
    <div class="justify-content-between">
        <img src="{{ $user->photo_uri }}" alt="" class="rounded-circle mr-2">
        {{ $user->name }}
    </div>
    <span class="badge badge-primary badge-pill"></span>
</li>
