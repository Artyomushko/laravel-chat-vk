<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravel chat</title>
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
<div class="header container">
    <div class="row d-flex justify-content-between align-items-center">
        <div class="col-3">

        </div>
        <div class="align-content-center">
            <h1>Laravel chat</h1>
        </div>
        <div class="col-3 text-right">
            <button class="btn btn-light" form="logout-form" type="submit">Logout</button>
            <form action="{{ route('auth.logout') }}" method="post" style="display: none;" id="logout-form">
                @csrf
            </form>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-3">
            <li class="list-group-item d-flex justify-content-between align-items-center" data-user-id="0">
                <div>
                    {{--<img src="{{ $user->photo_uri }}" alt="" class="rounded-circle">--}}
                    Common chat
                </div>
                <span class="badge badge-primary badge-pill"></span>
            </li>
            @foreach($users as $user)
                @include('layouts.interlocutor', ['user' => $user])
            @endforeach
        </div>
        <div class="col-9 bg-dark">

        </div>
    </div>
</div>

</body>
</html>
