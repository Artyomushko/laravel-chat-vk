<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageMark extends Model
{
    protected $fillable = [
        'value',
        'marker_id',
    ];

    public function message()
    {
        return $this->hasOne(Message::class);
    }

    public function marker()
    {
        return $this->hasOne(User::class, 'marker_id');
    }
}
