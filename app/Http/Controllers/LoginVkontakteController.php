<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Socialite;

class LoginVkontakteController extends Controller
{
    public function redirectToVk()
    {
        return Socialite::driver('vkontakte')->redirect();
    }

    public function VkCallback()
    {
        $vk_user = Socialite::driver('vkontakte')->user();
        $user = User::updateOrCreate(['vk_id' => $vk_user->id],
            [
                'name' => $vk_user->name,
                'photo_uri' => $vk_user->avatar,
            ]);
//        dd($vk_user);
        Auth::login($user);
        return redirect(route('chat.index'));
    }
}
