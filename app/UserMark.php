<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMark extends Model
{
    protected $fillable = [
        'value',
        'marker_id',
    ];

    /**
     * Return User whom marked
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }

    /**
     * Return User who marked
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function marker()
    {
        return $this->hasOne(User::class, 'marker_id');
    }
}
