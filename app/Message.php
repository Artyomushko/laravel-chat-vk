<?php

namespace App;

use App\Events\NewMessage;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Message
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int|null $from_user_id
 * @property int|null $to_user_id
 * @property float $rating
 * @property string $text
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereFromUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereToUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereUpdatedAt($value)
 */
class Message extends Model
{
    protected $fillable = [
        'from_user_id',
        'to_user_id',
        'text',
    ];

    protected $dispatchesEvents = [
        'created' => NewMessage::class,
    ];

    public function sender()
    {
        return $this->belongsTo(User::class, 'from_user_id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class, 'to_user_id');
    }

    private function marks()
    {
        return $this->hasMany(MessageMark::class);
    }

    private function countRating()
    {
        $this->rating = $this->marks()->avg('value');
        $this->save();
    }

    public function createMark($value)
    {
        $this->marks()->create(['value' => $value, 'marker_id' => \Auth::id()]);
        $this->countRating();
    }
}
