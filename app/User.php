<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $photo_uri
 * @property int $vk_id
 * @property float $rating
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhotoUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereVkId($value)
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'vk_id',
        'photo_uri'
    ];

//    public function

    private function marks()
    {
        return $this->hasMany(UserMark::class);
    }

    private function countRating()
    {
        $this->rating = $this->marks()->avg('value');
        $this->save();
    }

    public function createMark($value)
    {
        $this->marks()->create(['value' => $value, 'marker_id' => \Auth::id()]);
        $this->countRating();
    }
}
